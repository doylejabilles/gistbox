import React from 'react';

const AddGists = (props) => {

    return(
        <form onSubmit={props.addGist} className="form-inline">
            <div className="form-group">
                <input type="text" className="form-control" placeholder="add Gist" onChange={props.onKeyPress} value={props.username}></input>

                <input type="submit" className="btn btn-success"></input>
            </div>
        </form>
    )

}

export default AddGists;