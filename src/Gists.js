import React from 'react';
import GistItems from './GistItems';
import './index.css';

const Gists = (props) => {
    let gists = '';

    if( props.gists.length > 0 ) {
        gists = props.gists.map( (item, index) => {
            return(
                <GistItems
                username={item.username}
                url={item.url}
                key={index}
                />
            )
        });
    } else {
        gists = 'No Record Found';
    }


    return(
        <div>
            {gists}
        </div>
    )
};

export default Gists;