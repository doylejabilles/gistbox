import React from 'react';

const GistItems = (props) => {
    return(
        
        <div className="alert alert-success noPadding noMargin">{props.username}'s latest gist is <a href={props.url} target="blank">here</a>.</div>
    
    )
}

export default GistItems;