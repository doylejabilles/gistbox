import React, { Component } from 'react';
import Gists from './Gists';
import AddGists from './AddGists';

class GistBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            gists: [],
            username: '',
            isExisting: true,
        }
    };

    addGist = (e) => {
        e.preventDefault();
        let username = this.state.username;
        let url = `https://api.github.com/users/${username}/gists`;
        
        $.get(url, (result) => {
            if (result.length < 1 ) {
                let isExisting = false;
                
                this.setState({ isExisting });

                return;
            }
            username = result[0].owner.login;
            url = result[0].html_url;

            let gists = this.state.gists.concat({ username, url });
            this.setState({gists});
        });
        username = '';
        let isExisting = true;
        this.setState({username, isExisting});
    }

    onKeyPress = (e) => {
        let username = e.target.value;
        this.setState({ username })
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="row text-center">
                    <div className="col-md-12">
                        { ( !this.state.isExisting ) ?
                        <div className="alert alert-danger">
                            User don't exist.
                        </div>
                        : 
                        <div>
                        
                        </div>
                        }
                        
                    </div>
                    <div className="col-md-12">
                        <AddGists
                        addGist={this.addGist}
                        onKeyPress={this.onKeyPress}
                        username={this.state.username}
                        />
                    </div>
                    <div className="col-md-12">
                        <Gists
                            gists={ this.state.gists }
                        />
                    </div>
                </div>
            </div>
        )
    };


}
export default GistBox;