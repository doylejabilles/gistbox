import React from 'react';
import ReactDOM from 'react-dom';
import GistBox from './GistBox';

const destination = document.getElementById('app');

ReactDOM.render(<GistBox/>,destination);